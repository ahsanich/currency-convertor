# Mobile Developer Challenge

#### AppDemo @ [project_root]/appDemo/20210613_170217.mp4

## CheckList:

#### Develop a Currency Conversion App that allows a user view exchange rates for any given currency

- [x] Create a Project for a Mobile Phone
- [x] Android: _Kotlin_ | iOS: _Swift_ (sorry, no Objective-C or Java please! You can learn Kotlin/Swift easily I'm sure:))

### Functional Requirements:
- [x] Exchange rates must be fetched from: https://currencylayer.com/documentation  
- [x] Use free API Access Key for using the API
- [x] User must be able to select a currency from a list of currencies provided by the API(for currencies that are not available, convert them on the app side. When converting, floating-point error is accpetable)
- [x] User must be able to enter desired amount for selected currency
- [x] User should then see a list of exchange rates for the selected currency
- [x] Rates should be persisted locally and refreshed no more frequently than every 30 minutes (to limit bandwidth usage)
- [ ] Write unit testing

### UI Suggestion:
- [x] Some way to select a currency
- [x] Some text entry widget to enter the amount
- [x] A list/grid of exchange rates
- [x] It doesn’t need to be super pretty, but it shouldn’t be broken as well ;)

### What we're looking for:
- [x] An App that meets the Functional Requirements above
- [x] Your coding style! Show us how you like to write your code
- [x] Architecture, how you've structured your code
- [x] Principles, how you belive code should be written
- [x] Qualities, how you guarantee your code is functioning
