package com.playground.exr.adapter;

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.playground.exr.databinding.ItemCurrencyBinding
import com.playground.exr.model.Quote

class ExchangeRecyclerAdapter constructor(
    val owner: LifecycleOwner,
    val liveDataList: List<LiveData<Quote>> = listOf()
) : RecyclerView.Adapter<ExchangeRecyclerAdapter.CurrencyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = ItemCurrencyBinding.inflate(LayoutInflater.from(parent.context))
        return CurrencyViewHolder(owner, binding)
    }

    override fun onViewRecycled(holder: CurrencyViewHolder) {
        holder.detachObserver()
        super.onViewRecycled(holder)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.liveQuote = liveDataList[position]
    }

    override fun getItemCount(): Int = liveDataList.size

    //  ############################################################################################

    class CurrencyViewHolder(
        private val owner: LifecycleOwner,
        private val binding: ItemCurrencyBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        var liveQuote: LiveData<Quote>? = null
            set(value) {
                field = value
                value?.observe(owner, {
                    binding.labelCurrency.text = it.foreign
                    binding.labelAmount.text = "${it.finalRate}"
                })
            }

        fun detachObserver() {
            liveQuote?.removeObservers(owner)
        }

    }
}