package com.playground.exr

import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import com.playground.exr.adapter.ExchangeRecyclerAdapter
import com.playground.exr.databinding.ActivityMainBinding
import com.playground.exr.viewmodel.QuoteViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    private lateinit var mBinding: ActivityMainBinding

    private val quoteViewModel: QuoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.recyclerView.layoutManager = GridLayoutManager(this, 3)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()

        quoteViewModel.liveLocalCurrencyQty.observe(this, {
            mBinding.fieldLocalCurrency.setText("$it")
        })

        quoteViewModel.liveLocalCurrency.observe(this, { (index, _) ->
            run {
                mBinding.dropdownCurrencies.setSelection(index)
            }
        })

        quoteViewModel.liveCurrencies.observe(this, {
            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, it)
                .also {
                    mBinding.dropdownCurrencies.adapter = it
                }
        })

        quoteViewModel.liveQuotes.observe(this, {
            mBinding.recyclerView.adapter = ExchangeRecyclerAdapter(this, it)
        })

        mBinding.fieldLocalCurrency.doAfterTextChanged { text: Editable? ->
            val amount = text.toString().toFloatOrNull()
            quoteViewModel.convertibleAmount = amount ?: 1F
        }

        mBinding.dropdownCurrencies.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>?, view: View?, position: Int, id: Long
                ) {
                    quoteViewModel.liveCurrencies.value?.get(position)?.let {
                        quoteViewModel.defaultCurrency = it
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // Nothing to do here
                }

            }
    }

    override fun onPause() {
        quoteViewModel.liveQuotes.removeObservers(this)
        quoteViewModel.liveCurrencies.removeObservers(this)
        quoteViewModel.liveLocalCurrency.removeObservers(this)
        quoteViewModel.liveLocalCurrencyQty.removeObservers(this)

        super.onPause()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.refresh_quotes -> {    // Request latest quotes}
                quoteViewModel.updateQuotes()
                Toast.makeText(this, "Refreshing quotes...", Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}