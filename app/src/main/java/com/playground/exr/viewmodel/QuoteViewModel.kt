package com.playground.exr.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.playground.exr.api.LiveRateService
import com.playground.exr.database.dao.QuoteDao
import com.playground.exr.model.Quote
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class QuoteViewModel @Inject constructor(
    @Named("accessToken") private val accessToken: String,
    @Named("localCurrency") private val injected_defaultCurrency: String,
    private val liveRateService: LiveRateService,
    private val quoteQao: QuoteDao
) : ViewModel() {

    private val TAG = "ExchangeViewModel"

    private val mutableLocalCurrency = MutableLiveData<Pair<Int, String>>()
    private val mutableCurrencies = MutableLiveData<List<String>>()
    private val mutableLiveQuotes = MutableLiveData<List<MutableLiveData<Quote>>>()
    private val mutableLocalCurrencyAmount = MutableLiveData<Float>()

    val liveQuotes: LiveData<List<MutableLiveData<Quote>>>
        get() = mutableLiveQuotes

    val liveCurrencies: MutableLiveData<List<String>>
        get() = mutableCurrencies

    val liveLocalCurrency: MutableLiveData<Pair<Int, String>>
        get() = mutableLocalCurrency

    val liveLocalCurrencyQty: LiveData<Float>
        get() = mutableLocalCurrencyAmount

    var convertibleAmount: Float = 1F
        set(value) {
            field = if (value > 1F) value else 1F
            val liveQuotes: List<MutableLiveData<Quote>>? = mutableLiveQuotes.value
            liveQuotes?.forEach {
                it.value?.amount = field
                it.postValue(it.value)
            }
        }

    var defaultCurrency: String = injected_defaultCurrency
        set(value) {
            field = value
            liveCurrencies.value?.let {
                mutableLocalCurrency.value = (it.indexOf(field) to field)
            }
            
            val liveQuotes: List<MutableLiveData<Quote>>? = mutableLiveQuotes.value

            //  Finding the value for Factor by:
            //  1 / Default_or_Foreign_Currency

            //  Finding Quote for the Selected_or_Default currency
            val quote: Quote? =
                liveQuotes?.map { it.value } // mapping to Quote from MutableLiveDate<Quote>
                    ?.find { quote ->
                        quote?.foreign == field
                    }

            //  Changing the Factor value for each Quote (MutableLiveData<Quote>)
            quote?.let {
                liveQuotes.forEach { mutableLiveQuote ->
                    mutableLiveQuote.value
                        ?.apply { factor = (1F / quote.difference) }
                        .let { mutableLiveQuote.postValue(it) }
                }
            }
        }

    init {
        mutableLocalCurrencyAmount.postValue(convertibleAmount)
        updateQuotes()
    }

    fun updateQuotes() {
        viewModelScope.launch {
            Log.i(TAG, "AccessToken: $accessToken")

            Log.i(TAG, "Trying to get quotes from local db")
            var quotes = quoteQao.getAll(defaultCurrency)

            // Refresh if expires (limit 30 minutes)
            val life = 30L * 60L * 1000L
            val isExpired = quotes.any { (it.fetchedAt + life) < Date().time }

            if (quotes.isEmpty() || isExpired) {
                if (isExpired) Log.i(TAG, "Quotes are old")
                Log.i(TAG, "Trying to get quotes from API")

                liveRateService.getLiveRate(accessToken).let {
                    if (it.isSuccessful) it.body()?.let { it -> quotes = it.quotes.toList() }
                    Log.i(TAG, "Trying to save quotes in local db")

                    quoteQao.insert(quotes)     // Save API result in DB for future
                }
            }

            Log.i(TAG, "Quote List size: ${quotes.size}")

            //  Adjusting the quote according to Amount & Factor
            val amount = convertibleAmount
            val factor = (1F / (quotes.find { it.foreign == defaultCurrency }?.difference ?: 1F))
            quotes.forEach { quote ->
                quote.amount = amount
                quote.factor = factor
            }

            //  Notifying Quotes to UI
            mutableLiveQuotes.value.let {
                if (it == null) {   //  Quotes were never posted; post it
                    val liveQuotes = quotes.map { quote ->
                        val mutableLiveQuote = MutableLiveData<Quote>()
                        mutableLiveQuote.postValue(quote)
                        mutableLiveQuote
                    }
                    mutableLiveQuotes.postValue(liveQuotes)
                } else {
                    //  Finding & using previously posted quote MutableLiveData<Quote> to update with new value
                    //  This prevents the recycler view to lost it's view state when data is refreshed
                    quotes.forEach { quote ->
                        it.find { it.value == quote }?.postValue(quote)
                    }
                }
            }

            quotes.map { it.foreign } // Map to List of Foreign Currencies (String)
                .let {
                    //  Notify Spinner consumer to update adapter
                    //  Notify new value(it) if no value previously set
                    mutableCurrencies.postValue(mutableCurrencies.value ?: it)

                    //  Notify Spinner consumer to display a selected_or_default value
                    //  Notify new value(it) if no value previously set
                    mutableLocalCurrency.postValue(
                        mutableLocalCurrency.value ?: it.indexOf(defaultCurrency) to defaultCurrency
                    )
                }
        }
    }

}