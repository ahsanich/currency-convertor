package com.playground.exr.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.playground.exr.database.dao.QuoteDao
import com.playground.exr.model.Quote

@Database(entities = [Quote::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun quoteDao(): QuoteDao

}