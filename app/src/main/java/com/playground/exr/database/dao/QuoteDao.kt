package com.playground.exr.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.playground.exr.model.Quote

@Dao
interface QuoteDao {

    @Query("SELECT * FROM quote WHERE local LIKE :source")
    suspend fun getAll(source: String): List<Quote>


    @Query("SELECT `foreign` FROM quote")
    suspend fun getAllCurrencies(): List<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(quotes: List<Quote>)

}