package com.playground.exr.api

import com.playground.exr.api.response.LiveRate
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LiveRateService {

    @GET("live?")
    suspend fun getLiveRate(
        @Query("access_key") accessToken: String,
        @Query("source") source: String = "USD"
    ): Response<LiveRate>

}