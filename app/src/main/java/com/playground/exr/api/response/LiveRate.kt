package com.playground.exr.api.response

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.playground.exr.model.Quote
import java.lang.reflect.Type
import java.util.*

data class LiveRate(
    @SerializedName("success") var success: Boolean = false,
    @SerializedName("terms") var terms: String? = null,
    @SerializedName("privacy") var privacy: String? = null,
    @SerializedName("timestamp") var timestamp: Long? = null,
    @SerializedName("source") var source: String? = null,
    @SerializedName("quotes") @JsonAdapter(QuotationDeserializer::class) var quotes: Set<Quote> = setOf(),
) {
    override fun toString(): String {
        return "$success | $terms | $privacy | $timestamp | $source | $quotes"
    }
}

object QuotationDeserializer : JsonDeserializer<Set<Quote>> {

    override fun deserialize(
        json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?
    ): Set<Quote> {
        val quotes = mutableSetOf<Quote>()
        val fetchedAt = Date()

        (json as JsonObject).entrySet()
            .filter { mutableEntry -> mutableEntry.key.length == 6 }
            .map { mutableEntry -> (mutableEntry.key.take(3) to mutableEntry.key.takeLast(3)) to mutableEntry.value.asFloat }
            .forEach { (relativeCurrency: Pair<String, String>, diff: Float) ->
                quotes.add(Quote(relativeCurrency, diff, fetchedAt.time))
            }
        return quotes
    }
}