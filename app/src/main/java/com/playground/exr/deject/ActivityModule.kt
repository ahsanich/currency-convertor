package com.playground.exr.deject

import com.playground.exr.api.LiveRateService
import com.playground.exr.database.dao.QuoteDao
import com.playground.exr.viewmodel.QuoteViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Singleton
    @Provides
    fun provideQuoteViewModel(
        @Named("accessToken") accessToken: String,
        @Named("localCurrency") localCurrency: String,
        quoteDao: QuoteDao,
        liveRateService: LiveRateService
    ): QuoteViewModel =
        QuoteViewModel(accessToken, localCurrency, liveRateService, quoteDao)

}