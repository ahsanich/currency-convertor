package com.playground.exr.deject

import android.content.Context
import androidx.room.Room
import com.playground.exr.api.LiveRateService
import com.playground.exr.database.AppDatabase
import com.playground.exr.database.dao.QuoteDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Named("localCurrency")
    fun provideDefaultLocalCurrency() = "USD"

    @Provides
    @Named("baseURL")
    fun provideBaseURL() = "http://api.currencylayer.com"

    @Provides
    @Named("accessToken")
    fun provideAccessToken() = "289f550c28b7e288e677de1e0220ab3f"

    @Singleton
    @Provides
    fun provideApiService(@Named("baseURL") baseUrl: String): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    @Singleton
    @Provides
    fun provideLiveRateService(endPoint: Retrofit): LiveRateService =
        endPoint.create(LiveRateService::class.java)

    //  ############################################################################################
    //  ######################################    Database    ######################################
    //  ############################################################################################

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "${context.packageName}_db")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideQuoteDao(appDatabase: AppDatabase): QuoteDao = appDatabase.quoteDao()

}