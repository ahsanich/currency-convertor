package com.playground.exr.model

import androidx.room.*
import java.util.*

@Entity(tableName = "quote", indices = [Index(value = ["local", "foreign"], unique = true)])
data class Quote(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "local") val local: String,
    @ColumnInfo(name = "foreign") val foreign: String,
    @ColumnInfo(name = "difference") val difference: Float,
    @ColumnInfo(name = "fetchedAt") val fetchedAt: Long,
) {

    @Ignore
    var amount: Float = 1F

    @Ignore
    var factor: Float = 1F

    val finalDifference: Float
        get() = factor * difference

    val finalRate: Float
        get() = amount * finalDifference

    @Ignore
    constructor(
        local: String,
        foreign: String,
        standardDifference: Float,
        fetchedAt: Long
    ) : this(
        null, //
        local,
        foreign,
        standardDifference,
        fetchedAt
    )

    @Ignore
    constructor(toFrom: Pair<String, String>, standardDifference: Float, fetchedAt: Long) : this(
        toFrom.first,
        toFrom.second,
        standardDifference,
        fetchedAt
    )

    override fun toString(): String {
        return "$fetchedAt | $local to $foreign = $difference"
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Quote) {
            return false;
        }
        return "${local}${foreign}" == "${other.local}${other.foreign}"
    }

    override fun hashCode(): Int {
        return Objects.hash(local) + Objects.hash(foreign)
    }
}